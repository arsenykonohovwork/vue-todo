import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from '@/components/Dashboard';
import Employee from '@/components/Employee';
import NewEmployee from '@/components/NewEmployee';
import EditEmployee from '@/components/EditEmployee';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/new',
      name: 'new-employee',
      component: NewEmployee,
    },
    {
      path: '/edit/:empl_Id',
      name: 'edit-employee',
      component: EditEmployee,
    },
    {
      path: '/:empl_Id',
      name: 'employee',
      component: Employee,
    },
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
    },
  ],
});
